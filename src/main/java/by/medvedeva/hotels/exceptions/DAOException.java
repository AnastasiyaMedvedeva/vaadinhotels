package by.medvedeva.hotels.exceptions;

import org.apache.log4j.Level;

import lombok.extern.log4j.Log4j;

/**
 * @author Anastasiya Medvedeva
 *
 */
@Log4j
public class DAOException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor without parameters
	 */
	public DAOException() {
	}

	/**
	 * @param message
	 */
	public DAOException(String message) {
		super(message);
	}

	public DAOException(String msg, Throwable e) {
		super(msg, e);
		log.log(Level.ERROR, msg, e);
	}

	/**
	 * constructor
	 * 
	 * @param msg
	 * @param e
	 * @param clazz
	 *
	 */
	public DAOException(Class<?> clazz, String msg, Throwable e) {
		super(msg, e);
		log.log(Level.ERROR, msg, e);
	}
}