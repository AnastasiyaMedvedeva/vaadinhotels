package by.medvedeva.hotels;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.entity.Guaranty;

public class GuarantyField extends CustomField<Guaranty> {
	private static final long serialVersionUID = 1L;
	private static final String CREDIT_CARD_PAYMENT = "Credit card";
	private static final String CASH_PAYMENT = "Cash";
//Have a radio button group with some items
	RadioButtonGroup<String> paymentMethod = new RadioButtonGroup<>();
	Label cash = new Label("Payment will be made directly in the hotel");
	private Guaranty value = new Guaranty();
	private String caption = "Payment";
	private TextField guarantyDeposit = new TextField("Guaranty Deposit");
	private Binder<Guaranty> binder = new Binder<>(Guaranty.class);

	public GuarantyField(String caption) {
		super();
		this.caption = caption;
	}
	

	@Override
	protected Component initContent() {
		// layout for custom field
		HorizontalLayout layoutForGurantyField = new HorizontalLayout(paymentMethod);
		VerticalLayout fullField = new VerticalLayout(layoutForGurantyField);
		super.setCaption(caption);
		guarantyDeposit.setVisible(false);
		guarantyDeposit.setId("deposit");
		cash.setVisible(false);
		// set radiobutton parameters
		paymentMethod.setId("payment");
		paymentMethod.setItems(CREDIT_CARD_PAYMENT, CASH_PAYMENT);
		paymentMethod.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		paymentMethod.setSelectedItem(CASH_PAYMENT);
		// event behavior
		paymentMethod.addValueChangeListener(event -> howToPay(event));
		// read value of text field, and set it to Guaranty value;
		guarantyDeposit.addValueChangeListener(l -> {
			bindFields();
			if (binder.isValid()) {
			setValue(new Guaranty(Integer.parseInt(l.getValue())));
			}
		});
		 
		//add component to layout
		fullField.addComponents(cash, guarantyDeposit);
		fullField.setMargin(false);

		return fullField;
	}


	private void bindFields() {
		binder.forField(guarantyDeposit).withConverter(new StringToIntegerConverter(0, "Only digits!")).withValidator(v -> ((v <= 100) && (v >= 0)), "The Guaranty should be digits from 0 to 100").bind(Guaranty::getPercent, Guaranty::setPercent);
	}

	/**
	 * This method describe events behavior. If payment Method choice "Credit
	 * card", form shows text field, where need to input value of Guaranty Deposit
	 * in percent else if payment Method choice is "Cash", form show the
	 * notification
	 * 
	 * @param event
	 */
	public void howToPay(com.vaadin.data.HasValue.ValueChangeEvent<String> event) {
		if (event.getValue().equals("Credit card")) {
			guarantyDeposit.setVisible(true);
		} else if (event.getValue().equals("Cash")) {
			cash.setVisible(true);
			setValue(new Guaranty(0));
		}
	}

	@Override
	protected void doSetValue(Guaranty value) {
		try {
			binder.writeBean(value);
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.value = new Guaranty(value.getPercent());
	}


	@Override
	public Guaranty getValue() {
		return value;
	}



}
