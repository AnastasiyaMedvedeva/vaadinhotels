package by.medvedeva.hotels.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMUtil {
	private static EMUtil util;
	private EntityManagerFactory factory;
	private static EntityManager em;

	public static EMUtil getInstance(){
    if (util == null){
        util = new EMUtil();
    }
    return util;
}
	
	public EntityManager getEntityManager() {
		if (em == null) {
		factory = Persistence.createEntityManagerFactory("demo_hotels");
			em = factory.createEntityManager();
		}
		return em;
 }

	
}
