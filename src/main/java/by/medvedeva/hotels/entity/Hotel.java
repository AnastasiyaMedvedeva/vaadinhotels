package by.medvedeva.hotels.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@NamedQueries({ @NamedQuery(name = "hotel.findAll", query = "SELECT h FROM Hotel AS h"), @NamedQuery(name = "hotel.findByName", query = "SELECT h FROM Hotel AS h WHERE (LOWER(h.name) LIKE :filter)"),
		@NamedQuery(name = "hotel.findByAddress", query = "SELECT h FROM Hotel AS h WHERE (LOWER(h.address) LIKE :filter)"),
		@NamedQuery(name = "hotel.findByNameAndAddress", query = "SELECT h FROM Hotel AS h WHERE (LOWER(h.name) LIKE :filterN) AND (LOWER(h.address) LIKE :filterA)") })
@Table(name = "HOTEL")
public class Hotel extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Name is required")
	private String name = "";

	@NotNull(message = "Address is required")
	private String address = "";

	@NotNull(message = "Rating is required")
	@Pattern(regexp = "^([1-6]{1,1})$", message = "Rating should be a positive number less then 6")
	private Integer rating = 1;

	@NotNull(message = "Operates From is required")
	@Column(name = "OPERATES_FROM")
	private Long operatesFrom = (long) 1;

	@ManyToOne
	private Category category;


	@NotNull(message = "URL is required")
	@Pattern(regexp = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$", message = "URL-address expected there")
	private String url = "www";

	@Column(name = "DESCRIPTION")
	private String description;

	@Embedded
	@Column(name = "GUARANTY_FEE")
	private Guaranty guarantyFee = new Guaranty();


	@Override
	public String toString() {
		return name + " " + rating + "stars " + address;
	}


	public Hotel(String name, String address, Integer rating, Long operatesFrom, Category category, String url, String description, Guaranty guarantyFee) {
		super();
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.url = url;
		this.description = description;
		this.guarantyFee = guarantyFee;
	}




	public Hotel() {
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *          the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the rating
	 */
	public Integer getRating() {
		return rating;
	}

	/**
	 * @param rating
	 *          the rating to set
	 */
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	/**
	 * @return the operatesFrom
	 */
	public Long getOperatesFrom() {
		return operatesFrom;
	}

	/**
	 * @param operatesFrom
	 *          the operatesFrom to set
	 */
	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}


	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}


	/**
	 * @param category
	 *          the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *          the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the guarantyFee
	 */
	public Guaranty getGuarantyFee() {
		return guarantyFee;
	}


	/**
	 * @param guarantyFee
	 *          the guarantyFee to set
	 */
	public void setGuarantyFee(Guaranty guarantyFee) {
		this.guarantyFee = guarantyFee;
	}

}
