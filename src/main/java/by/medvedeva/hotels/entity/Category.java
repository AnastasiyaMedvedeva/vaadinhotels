package by.medvedeva.hotels.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * @author Anastasiya Medvedeva
 * 
 *         Class Category created for persistent hotels category
 *
 */
@Entity
@NamedQueries({ @NamedQuery(name = "сategory.findAll", query = "SELECT c FROM Category AS c"), @NamedQuery(name = "category.findByName", query = "SELECT c FROM Category AS c WHERE LOWER(c.nameCategory) LIKE :filter") })
@Table(name = "CATEGORY")
public class Category extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "NAME")
	private String nameCategory = "";

	public Category(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public Category() {
	}

	/**
	 * @return the nameCategory
	 */
	public String getNameCategory() {
		return nameCategory;
	}

	/**
	 * @param nameCategory
	 *          the nameCategory to set
	 */
	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}


	@Override
	public String toString() {
		return nameCategory;
	}


}
