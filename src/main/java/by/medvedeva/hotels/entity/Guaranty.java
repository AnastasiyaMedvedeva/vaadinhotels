package by.medvedeva.hotels.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class Guaranty implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	private Integer percent = 0;
	
	

}
