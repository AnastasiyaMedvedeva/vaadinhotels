package by.medvedeva.hotels;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import by.medvedeva.hotels.entity.Category;
import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.exceptions.DAOException;
import by.medvedeva.hotels.service.CategoryService;
import by.medvedeva.hotels.service.Service;
import by.medvedeva.hotels.views.HotelView;

//public class HotelForm extends HotelFormDesign {
public class HotelForm extends FormLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(HotelForm.class.getName());
	private NativeSelect<Category> category = new NativeSelect<>("Category");
	// protected GuarantyField guarantyFee = new GuarantyField("Guaranty Fee");
	GuarantyField guarantyField = new GuarantyField("Means of payment");
	private DateField operatesFrom = new DateField("Operates From");
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	private TextField address = new TextField("Address");
	private TextArea description = new TextArea("Description");
	private TextField rating = new TextField("Rating");
	private TextField name = new TextField("Name");
	private TextField url = new TextField("URL");
	private Button save = new Button("Save");
	private Service service = Service.getInstance();
	private HotelView hotelUI;
	private Hotel hotel;

	public HotelForm(HotelView hotelUI) {
		this.hotelUI = hotelUI;
		setSizeUndefined();
		HorizontalLayout nameAndAddress = new HorizontalLayout(name, address);
		HorizontalLayout hor1 = new HorizontalLayout(category, rating);
		VerticalLayout ver = new VerticalLayout(nameAndAddress, url, hor1, operatesFrom, guarantyField, description, save);
		ver.setMargin(false);
		name.setId("hotelName");
		address.setId("hotelAddress");
		operatesFrom.setId("operatesFrom");
		url.setId("url");
		category.setId("hotelCategory");
		rating.setId("rating");
		url.setSizeFull();
		operatesFrom.setSizeFull();
		guarantyField.setSizeFull();
		guarantyField.addValueChangeListener(l -> {
			// guarantyField.paymentMethod.getValue();
			Notification.show("You change Guaranty deposit from" + l.getOldValue() + "to" + l.getValue());
		});
		updateCategory();
		save.setClickShortcut(KeyCode.ENTER);
		save.setSizeFull();
		save.setId("save");
		bindFields();
		save.addClickListener(e -> this.save());
		addComponents(ver);
	}

	public HotelForm() {

	}

	/**
	 * this metod will update category native select on hotels list form
	 */
	public void updateCategory() {
		category.setItems(CategoryService.getInstance().findAllCategory());
	}

	public Hotel readOneHotel(Hotel hotel) {
		this.hotel = hotel;
		binder.readBean(hotel);
		return hotel;
	}

	// read the hotels from grid or form
	public List<Hotel> readHotels(Set<Hotel> selectedHotels) {
		List<Hotel> listHotels = new ArrayList<>();
		if (selectedHotels.isEmpty()) {
			LOGGER.warning("empty listSelect from UI");
		} else {
			selectedHotels.forEach(h -> {
				binder.readBean(h);
				listHotels.add(h);
			});
			setVisible(true);
			name.selectAll();
		}
		return listHotels;
	}

	// fields can't be null or empty
	private void bindFields() {
		binder.forField(rating).withConverter(new StringToIntegerConverter(0, "Only digits!")).asRequired("Field can't be empty").withValidator(v -> (v < 6 && v > 0), "Rating should be a positive number less then 6").bind(Hotel::getRating,
				Hotel::setRating);
		binder.forField(name).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getName, Hotel::setName);
		binder.forField(address).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(category).withValidator(v -> (v != null), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getCategory, Hotel::setCategory);
		binder.forField(url).withValidator(v -> (v != null && !v.isEmpty()), "Field can't be empty").asRequired("Field can't be empty").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(operatesFrom).asRequired("Field can't be empty").withConverter(new DateConverter()).withValidator(v -> (v >= 0), "Date should be today or  until today").bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
		binder.forField(guarantyField).bind(Hotel::getGuarantyFee, Hotel::setGuarantyFee);
	}

	// this method delete hotels, which user choices
	public void delete(List<Hotel> listHotels) throws DAOException {
		listHotels.forEach(h -> {
			try {
				service.delete(h);
			} catch (DAOException e) {
				LOGGER.warning("Couldn't delete hotel");
				e.printStackTrace();
			}
		});
		listHotels.clear();
		binder.removeBean();
		hotelUI.updateList();
		setVisible(false);
	}

	private void save() {
		try {
			binder.writeBean(hotel);
			if ((binder.isValid()) && (!checkforExist(hotel))) {
				service.createOrUpdate(hotel);
			}
			binder.removeBean();
			hotelUI.updateList();
			setVisible(false);
		} catch (DAOException e) {
			LOGGER.warning("Couldn't save hotel");
		} catch (ValidationException e1) {
			LOGGER.warning("Couldn't save hotel");
		} finally {
			setVisible(false);
		}
	}

	// This method checks if new hotel already exist
	private boolean checkforExist(Hotel hotel) {
		boolean isExist = false;
		try {
			List<Hotel> hotels = service.findAll();
			for (Hotel h : hotels) {
				if ((hotel.getAddress().equals(h.getAddress())) && (hotel.getCategory().equals(h.getCategory())) && (hotel.getName().equals(h.getName())) && (hotel.getOperatesFrom().equals(h.getOperatesFrom())) && (hotel.getRating() == h.getRating())
						&& (hotel.getUrl().equals(h.getUrl())) && (hotel.getId() != h.getId())) {
					isExist = true;
					Notification.show("Warning", "This hotel is already exist", Notification.Type.HUMANIZED_MESSAGE);
				}
			}
		} catch (DAOException e) {
			LOGGER.warning(" Couldn't chek hotel for exist");
		}
		return isExist;
	}

}
