package by.medvedeva.hotels;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.entity.Category;
import by.medvedeva.hotels.exceptions.DAOException;
import by.medvedeva.hotels.service.CategoryService;
import by.medvedeva.hotels.views.CategoryView;
import lombok.extern.log4j.Log4j;

@Log4j
public class CategoriesForm extends FormLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(CategoriesForm.class.getName());
	private TwinColSelect<Category> twinColSelect = new TwinColSelect<>("Hotel Categories");
	private CategoryService categoryService = CategoryService.getInstance();
	private TextField nameCategory = new TextField("New Category");
	private Button save = new Button("Save");
	private Button delete = new Button("Delete");
	private Binder<Category> binder = new Binder<>(Category.class);
	private Category category;
	private List<Category> list = new ArrayList<>();
	private Set<Category> selectedCategories;
	HotelForm hotelForm = new HotelForm();
	private CategoryView categoryUI;

	// конструктор формы, которая передается на страницу
	public CategoriesForm(CategoryView categoryUI) {
		this.categoryUI = categoryUI;
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, delete);
		twinColSelect.setItems(categoryService.findAllCategory());
		twinColSelect.setRightColumnCaption("Selected Categories");
		VerticalLayout form = new VerticalLayout(twinColSelect, nameCategory, buttons);
		updateCategoriesList();
		addComponents(form);
		save.setClickShortcut(KeyCode.ENTER);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		delete.setStyleName(ValoTheme.BUTTON_DANGER);
		delete.setVisible(true);
		delete.setEnabled(false);
		twinColSelect.addSelectionListener(event -> this.delete.setEnabled(true));
		save.addClickListener(e -> this.save());
		delete.addClickListener(e -> {
			this.delete();
			bindFields();
			nameCategory.clear();
		});
		bindFields();
	}

	private void bindFields() {
		binder.forField(nameCategory).bind(Category::getNameCategory, Category::setNameCategory);
	}

	public TwinColSelect<Category> updateCategoriesList() {
		twinColSelect.setItems(categoryService.findAllCategory());
		return twinColSelect;
	}

	public void setOneCategory(Category category) {
		this.category = category;
		binder.readBean(category);
		delete.setEnabled(categoryService.isPersisted(category));
		setVisible(true);
	}

	public List<Category> setCategories(Set<Category> selectCategories) {
		if (selectCategories.isEmpty()) {
			LOGGER.warning("empty listSelect from UI");
		} else {
			selectCategories.forEach( c ->  {
				binder.readBean(c);
				list.add(c);
				delete.setEnabled(categoryService.isPersisted(c));
			});
			setVisible(true);
		}
		return list;
	}

	private void delete() {
		selectedCategories = twinColSelect.getSelectedItems();
		list = setCategories(selectedCategories);
		list.forEach( c-> {
			try {
				categoryService.delete(c);
			} catch (DAOException e) {
				LOGGER.warning("Trouble with delet category");
			}
		}); 
		list.clear();
		categoryUI.updateCategoryList();
		hotelForm.updateCategory();
		setVisible(true);
	}

	
	private void save() {
		try {
			binder.writeBean(category);
			if (binder.isValid()) {
				categoryService.createOrUpdate(category);
				binder.removeBean();
				categoryUI.updateCategoryList();
				category = new Category();
			}
		} catch (DAOException e) {
			log.error("Transaction failed in createOrUpdate(category); method ", e);
		} catch (ValidationException e) {
			log.error("Transaction failed in createOrUpdate(category); method ", e);
		}
		nameCategory.clear();
		setVisible(true);
	}

}
