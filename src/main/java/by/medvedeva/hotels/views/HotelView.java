package by.medvedeva.hotels.views;

import java.util.List;
import java.util.logging.Logger;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.BulkUpdateForm;
import by.medvedeva.hotels.HotelForm;
import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.exceptions.DAOException;
import by.medvedeva.hotels.service.Service;

@SpringView
public class HotelView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;
	public static String VIEW_NAME = "hotels";
	private static final Logger LOGGER = Logger.getLogger(HotelView.class.getName());
	private Service service = Service.getInstance();
	public Grid<Hotel> grid = new Grid<>(Hotel.class);
	private TextField filterName = new TextField();
	private TextField filterAddress = new TextField();
	private HotelForm hotelForm = new HotelForm(this);
	private BulkUpdateForm bulk = new BulkUpdateForm(this);
	private PopupView popup = new PopupView(null, bulk);
	Hotel hotel;
	List<Hotel> hotels;

	@Override
	public void enter(ViewChangeEvent event) {
		HorizontalLayout toolbar = new HorizontalLayout();
		initToolbar(toolbar);
		HorizontalLayout filterPannel = new HorizontalLayout();
		initFilterPanel(filterPannel);
		initGridComponent();
		popup.setSizeFull();
		VerticalLayout bulkAndGrid = new VerticalLayout(toolbar, filterPannel, popup, grid);
		// hide form by default
		hotelForm.setVisible(false);

		HorizontalLayout main = new HorizontalLayout(bulkAndGrid, hotelForm);
		main.setSizeFull();
		main.setExpandRatio(bulkAndGrid, 1);

		addComponents(main);

	}

	private void initToolbar(HorizontalLayout toolbar) {
		Button newHotel = new Button("New Hotel");
		newHotel.setId("newHotel");
		newHotel.addClickListener(e -> {
			hotelForm.setVisible(true);
			hotelForm.readOneHotel(new Hotel());
		});
		Button editHotel = new Button("Edit Hotel");
		editHotel.addClickListener(e -> {
			hotelForm.setVisible(true);
			if (!grid.getSelectedItems().isEmpty()) {
				hotelForm.readOneHotel(grid.getSelectedItems().iterator().next());
			}
		});
		Button deleteHotel = new Button("Delete Hotel");
		deleteHotel.addClickListener(e -> {
			try {
				hotelForm.delete(hotelForm.readHotels(grid.getSelectedItems()));
			} catch (DAOException e1) {
				LOGGER.warning("Couldn't delete hotel");
			}
		});

		Button bulkUp = new Button("Bulk Update");
		bulkUp.addClickListener(e -> {
			popup.setPopupVisible(true);
		});

		toolbar.addComponents(newHotel, editHotel, deleteHotel, bulkUp);

	}

	private void initGridComponent() {
		grid.setSelectionMode(SelectionMode.MULTI);
		grid.setColumns("name", "address", "rating", "operatesFrom", "category", "guarantyFee");
		grid.addColumn(Hotel::getDescription).setCaption("Description").setResizable(true);
		grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "' target='_blank'>" + hotel.getName() + "</a>", new HtmlRenderer()).setCaption("Link").setResizable(true);
		grid.setSizeFull();
		updateList();
		hotelForm.updateCategory();
	}

	public void updateList() {
		List<Hotel> hotels;
		try {
			hotels = service.findAll();
			grid.setItems(hotels);
		} catch (DAOException e) {
			LOGGER.warning("Couldn't get hotel");
		}
	}

	private void initFilterPanel(HorizontalLayout filterPannel) {
		filterName.setId("filterName");
		filterName.setPlaceholder("filter by name...");
		filterAddress.setPlaceholder("filter by address...");
		filterName.addValueChangeListener(e -> filterEvent());
		filterAddress.addValueChangeListener(e -> filterEvent());
		filterName.setValueChangeMode(ValueChangeMode.LAZY);
		filterAddress.setValueChangeMode(ValueChangeMode.LAZY);

		Button clearFilterTextBtn = new Button(VaadinIcons.CLOSE);
		clearFilterTextBtn.setDescription("clear the current filter");
		clearFilterTextBtn.addClickListener(e -> filterName.clear());
		clearFilterTextBtn.addClickListener(e -> filterAddress.clear());

		CssLayout filtering = new CssLayout();
		filtering.addComponents(filterName, filterAddress, clearFilterTextBtn);
		filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		filterPannel.addComponents(filtering);
	}

	// this method for adding to grid Item, which matches with filter
	public void filterEvent() {
		try {
			grid.setItems(service.findByFilter(filterName.getValue(), filterAddress.getValue()));
		} catch (DAOException e) {
			LOGGER.warning("Couldn't get hotel");
		}
	}

}
