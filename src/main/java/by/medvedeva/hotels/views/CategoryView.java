package by.medvedeva.hotels.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;

import by.medvedeva.hotels.CategoriesForm;
import by.medvedeva.hotels.entity.Category;

@SpringView
public class CategoryView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;
	public static String VIEW_NAME = "categories";
	private CategoriesForm categoriesForm = new CategoriesForm(this);


	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout main = new VerticalLayout(categoriesForm);
		categoriesForm.setVisible(true);
		updateCategoryList();
		categoriesForm.setOneCategory(new Category());

		addComponents(main);

	}

	public void updateCategoryList() {
		categoriesForm.updateCategoriesList();
	}

}
