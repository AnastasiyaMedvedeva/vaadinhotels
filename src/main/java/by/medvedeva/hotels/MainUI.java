package by.medvedeva.hotels;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import by.medvedeva.hotels.views.CategoryView;
import by.medvedeva.hotels.views.HotelView;

@Theme("mytheme")
@SpringUI
public class MainUI extends UI {
	private static final long serialVersionUID = 1L;
	/**
	 * page title
	 */
	private static final String PAGE_TITLE = "Hotels";
	private MenuBar menuBar;
	private Navigator navigator;

	@WebListener
	public static class MyContextLoaderListener extends ContextLoaderListener {
	}

	@Configuration
	@EnableVaadin
	public static class MyConfiguration {
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		// set name of the page
		getPage().setTitle(PAGE_TITLE);

		VerticalLayout layout = new VerticalLayout();
		addStyleName(ValoTheme.UI_WITH_MENU);
		layout.setMargin(false);
		layout.setSpacing(false);
		layout.setId("layout");
		layout.setSizeFull();
		layout.addComponents(getMenuBar());

		// create panel for main layout
		Panel mainPanel = initMainPanel();
		layout.addComponent(mainPanel);
		layout.setExpandRatio(mainPanel, 100);

		// initialize navigator
		navigator = new Navigator(this, mainPanel);
		// register views
		navigator.addView(HotelView.VIEW_NAME, HotelView.class);
		navigator.addView(CategoryView.VIEW_NAME, CategoryView.class);

		// navigate to hotels
		navigator.navigateTo(HotelView.VIEW_NAME);

		super.setId("mainView");

		setSizeFull();
		setContent(layout);
	}

	private Component getMenuBar() {
		if (menuBar != null) {
			return menuBar;
		}
		// create menu bar
		menuBar = new MenuBar();
		// create menu items
		menuBar.addItem("Hotel", command -> navigator.navigateTo(HotelView.VIEW_NAME));
		menuBar.addItem("", null).setEnabled(false);
		menuBar.addItem("Categories", command -> navigator.navigateTo(CategoryView.VIEW_NAME));

		// menuBar.setStyleName(ValoTheme.MENUBAR_SMALL);

		return menuBar;
	}

	private Panel initMainPanel() {
		Panel mainPanel = new Panel();
		mainPanel.setSizeFull();
		mainPanel.setId("mainPanel");
		return mainPanel;
	}
	
	@WebServlet(urlPatterns = "/*", name = "MainUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MainUI.class, productionMode = false)
	public static class MainUIServlet extends VaadinServlet {
		private static final long serialVersionUID = 1L;
	}

}
