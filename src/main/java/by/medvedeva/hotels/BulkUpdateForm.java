package by.medvedeva.hotels;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import by.medvedeva.hotels.entity.Guaranty;
import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.exceptions.DAOException;
import by.medvedeva.hotels.service.CategoryService;
import by.medvedeva.hotels.service.Service;
import by.medvedeva.hotels.views.HotelView;

public class BulkUpdateForm extends FormLayout {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(CategoriesForm.class.getName());
	private TextField updateField = new TextField("New Value");
	NativeSelect<PopupSelect> select = new NativeSelect<>("Select Field");
	protected NativeSelect<by.medvedeva.hotels.entity.Category> category = new NativeSelect<>();
	Pattern pattern = Pattern.compile("^([1-6]{1,1})$");
	private Button updateButton = new Button("Update");
	private Button cancel = new Button("Cancel");
	private Panel panel = new Panel("Bulk Update");
	private Label error = new Label("Input value is not correct");
	private Service service = Service.getInstance();
	private HotelForm hotelForm = new HotelForm();
	private DateField operatesFrom = new DateField();
	private HotelView hotelUI;

	// конструктор формы, которая передается на страницу
	// public BulkUpdateForm(MyUI myUI) {
	// this.myUI = myUI;
	public BulkUpdateForm(HotelView hotelUI) {
		this.hotelUI = hotelUI;
		setSizeUndefined();
		select.setSizeFull();
		panel.setSizeFull();
		select.setItems(PopupSelect.values());
		category.setItems(CategoryService.getInstance().findAllCategory());
		HorizontalLayout buttons = new HorizontalLayout(updateButton, cancel);
		category.setVisible(false);
		operatesFrom.setVisible(false);
		updateField.setVisible(true);
		VerticalLayout form = new VerticalLayout(select, category, operatesFrom, updateField, buttons);
		form.setSizeFull();
		panel.setContent(form);
		error.setVisible(false);
		addComponents(panel, error);
		updateButton.addClickListener(e -> this.updateFields(hotelUI));
		select.addSelectionListener(event -> this.selectEvent());
	}

	/**
	 * 
	 */
	public void selectEvent() {
		if ((select.getValue().compareTo(PopupSelect.CATEGORY)) == 0) {
			category.setVisible(true);
			category.setSizeFull();
			updateField.setVisible(false);
		} else if ((select.getValue().compareTo(PopupSelect.OPERATES_FROM)) == 0) {
			updateField.setVisible(false);
			operatesFrom.setVisible(true);
			operatesFrom.setSizeFull();
			setVisible(true);
		}
	}

	/**
	 * @param myUI
	 */
	public void updateFields(HotelView hotelUI) {
		this.updateHotels(select.getValue());
		panel.setVisible(false);
		hotelUI.updateList();
		setVisible(true);
	}

	// This method updates hotels field, witch user selected
	public void updateHotels(PopupSelect value) {
		Set<Hotel> testList = hotelUI.grid.getSelectedItems();
		List<Hotel> list = hotelForm.readHotels(testList);
		list.forEach(h -> {
			switchFieldForUpdate(value, h);
			try {
				service.createOrUpdate(h);
			} catch (DAOException e) {
				LOGGER.warning("Couldn't update hotel");
			}
		});
	}

	/**
	 * @param value
	 * @param h
	 */
	public void switchFieldForUpdate(PopupSelect value, Hotel h) {
		switch (value) {
		case NAME:
			h.setName(updateField.getValue());
			break;
		case ADDRESS:
			h.setAddress(updateField.getValue());
			break;
		case URL:
			h.setUrl(updateField.getValue());
			break;
		case DESCRIPRION:
			h.setDescription(updateField.getValue());
			break;
		case RATING:
			updateRating(h);
			break;
		case CATEGORY:
			h.setCategory(category.getValue());
			break;
		case OPERATES_FROM:
			long days = Duration.between(operatesFrom.getValue().atTime(0, 0), LocalDate.now().atTime(0, 0)).toDays();
			h.setOperatesFrom(days);
			break;
		case GUARANTY:
			if ((null != (updateField.getValue())) && (Integer.parseInt(updateField.getValue()) < 100) && (Integer.parseInt(updateField.getValue()) > 0)) {
				Guaranty fee = new Guaranty();
				fee.setPercent(Integer.parseInt(updateField.getValue()));
				h.setGuarantyFee(fee);
			} else {
				Notification.show("The Guaranty should be digits from 0 to 100");
			}
			break;
		default:
			LOGGER.warning("unknown value for changhe");
			break;
		}
	}

	/**
	 * @param h
	 *          this method validate input rating and update it in hotel
	 */
	public void updateRating(Hotel h) {
		Matcher mat = pattern.matcher(updateField.getValue());
		if (mat.matches()) {
			h.setRating(Integer.parseInt(updateField.getValue()));
		} else {
			error.setVisible(true);
		}
	}

}
