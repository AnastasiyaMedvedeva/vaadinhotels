package by.medvedeva.hotels.service;

import java.util.ArrayList;
import java.util.List;

import by.medvedeva.hotels.entity.Category;
import by.medvedeva.hotels.entity.Hotel;
import by.medvedeva.hotels.exceptions.DAOException;
import lombok.extern.log4j.Log4j;

@Log4j
public class CategoryService extends AbstractService <Category> {
	private static CategoryService instance;
	

	private CategoryService() {
		super(Category.class);
	}

	public static CategoryService getInstance() {
		if (instance == null) {
			instance = new CategoryService();
			instance.ensureCategoryData();
		}
		return instance;
	}
	

	/**
	 * Get list of all categories
	 *
	 * @return list
	 * @throws DaoException
	 *           custom exception
	 */
	public List<Category> findAllCategory() {
		List<Category> listCategory = new ArrayList<>();
		try {
			if (!em.getTransaction().isActive()) {
				em.getTransaction().begin();
				listCategory = em.createNamedQuery("сategory.findAll", Category.class).getResultList();
				em.getTransaction().commit();
			} else {
				listCategory = em.createNamedQuery("сategory.findAll", Category.class).getResultList();
			}
		} catch (IllegalArgumentException e) {
			em.getTransaction().rollback();
			log.error("Transaction failed in service.findAllCategory() method ", e);
		}
		return listCategory;
	}
	
	public boolean isPersisted(Category category) {
		boolean persisted = false;
		List<Category> list = findAllCategory();
		for (Category cat : list) {
			if (cat.equals(category)) {
				persisted = true;
			}
		}
		return persisted;
	}
	
	/* Demo data generation... */
	private static final String[] categoryNames = new String[] { "Hotel", "Hostel", "GuestHouse", "Appartments" };

	public void ensureCategoryData() {
		try {
			if (findAllCategory().isEmpty()) {
				em.getTransaction().begin();
				for (String name : categoryNames) {
					createOrUpdate(new Category(name));
				}
				em.getTransaction().commit();
			}
		} catch (DAOException e) {
			em.getTransaction().rollback();
			log.error("Transaction failed in ensureCategoryData() method ", e);
		}
	}
	
	/**
	 * Remove Category from table
	 *
	 * @param category
	 *          object
	 * @throws DaoException
	 *           custom exception
	 */
	public void delete(Category category) throws DAOException {
		try {
			if (em.getTransaction().isActive()) {
				em.remove(em.find(Category.class, category.getId()));
			  List<Hotel> hotelsList = Service.getInstance().findAll();
			 hotelsList.forEach( hotel-> em.refresh(hotel));
			} else {
				em.getTransaction().begin();
				em.remove(em.find(Category.class, category.getId()));
				 List<Hotel> hotelsList = Service.getInstance().findAll();
				  hotelsList.forEach( hotel -> em.refresh(hotel)); 
				em.getTransaction().commit();
			}
		} catch (IllegalArgumentException e) {
			em.getTransaction().rollback();
			throw new DAOException("Fatal error in  delete method", e);
		}
	}
	
}
