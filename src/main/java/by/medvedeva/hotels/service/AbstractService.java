package by.medvedeva.hotels.service;

import javax.persistence.EntityManager;

import by.medvedeva.hotels.entity.AbstractEntity;
import by.medvedeva.hotels.exceptions.DAOException;
import by.medvedeva.hotels.utils.EMUtil;

public abstract class AbstractService<T extends AbstractEntity> {
	EntityManager em = EMUtil.getInstance().getEntityManager();
	private Class<?> persistentClass;

	protected AbstractService(Class<?> persistentClass) {
		this.persistentClass = persistentClass;
	}

	/**
	 * Get one entity from table by id
	 *
	 * @param id
	 *          - row id
	 * @return row object
	 * @throws DaoException
	 *           custom exception
	 */

	@SuppressWarnings("unchecked")
	public T getById(Long id) throws DAOException {

		T entity;
		try {
			em.getTransaction().begin();
			entity = (T) em.find(persistentClass, id);
			em.getTransaction().commit();
		} catch (IllegalArgumentException e) {
			em.getTransaction().rollback();
			throw new DAOException("Fatal error in get method", e);
		}
		return entity;
	}

	public void createOrUpdate(T entity) throws DAOException {
		try {
			if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
			if (null == entity.getId()) {
				em.persist(entity);
			} else {
				em.merge(entity);
			}
			em.getTransaction().commit();
			} else {
				if (null == entity.getId()) {
					em.persist(entity);
				} else {
					em.merge(entity);
				}
			}
		} catch (IllegalArgumentException e) {
			em.getTransaction().rollback();
			throw new DAOException("Fatal error in createOrUpdate method", e);
		}
	}

}
