package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumRun {
	private final static String BASE_URL = "http://localhost:8080";
	private final static String EXPECTED_URL = "http://localhost:8080/#!hotels";
	static String actualTitle = "";

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "D:/Java/geckodriver-v0.16.1-win64/geckodriver.exe");
		driver = new FirefoxDriver();

		// launch Fire fox and direct it to the Base URL
		driver.get(BASE_URL);

		// get the actual value of the title
		actualTitle = driver.getTitle();
		
		/*
     * compare the actual title of the page with the expected one and print
     * the result as "Passed" or "Failed"
     */
    if (actualTitle.contentEquals(EXPECTED_URL)){
        System.out.println("Test Passed!");
    } else {
        System.out.println("Test Failed");
    }
   
    //close Fire fox
    driver.close();
   
    // exit the program explicitly
    System.exit(0);
}

}
