package hotels;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class AbstractUITest {
	protected final static String BASE_URL = "http://localhost:8080";
	protected final static String EXPECTED_URL = "http://localhost:8080/#!hotels";
	WebDriver driver;
	
	public AbstractUITest() {
		System.setProperty("webdriver.gecko.driver", "D:/Java/geckodriver-v0.16.1-win64/geckodriver.exe");
	}

	@Before
	public void initDriver() throws InterruptedException {
		driver = new FirefoxDriver();
	}

	@After
	public void tearDown() throws InterruptedException {
		Thread.sleep(1000); // Let the user actually see something!
		driver.quit();
	}

}
