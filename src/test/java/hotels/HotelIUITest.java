package hotels;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HotelIUITest extends AbstractUITest {

	@Test
	public void testHotelsAdding() throws InterruptedException {
		driver.get(BASE_URL);
		Thread.sleep(1000); // Let the user actually see something!

	// go to newHotel page
		WebElement newHotel = driver.findElement(By.id("newHotel"));
		newHotel.click();
		Thread.sleep(1000);
		driver.findElement(By.id("hotelName")).clear();
		driver.findElement(By.id("hotelName")).sendKeys("One More Hotel");
		driver.findElement(By.id("hotelAddress")).clear();
		driver.findElement(By.id("hotelAddress")).sendKeys("Minsk City");
		driver.findElement(By.id("url")).clear();
		driver.findElement(By.id("url")).sendKeys("WWW");
		WebElement hotelCategory = driver.findElement(By.xpath("//*[@id='hotelCategory']/select"));
		Select selectedCategory = new Select(hotelCategory);
		selectedCategory.selectByValue("1");
		driver.findElement(By.id("rating")).clear();
		driver.findElement(By.id("rating")).sendKeys("2");
		driver.findElement(By.id("gwt-uid-41")).clear();
		driver.findElement(By.id("gwt-uid-41")).sendKeys("21.05.2015");

		WebElement paymentMethod = driver.findElement(By.id("gwt-uid-54"));
		paymentMethod.click();
		Thread.sleep(1000);
		driver.findElement(By.id("deposit")).sendKeys("24");
		Thread.sleep(1000);
		driver.findElement(By.id("save")).click();
		Thread.sleep(1000); // Let the user actually see something!
		driver.findElement(By.id("filterName")).sendKeys("One More Hotel");
		Thread.sleep(2000);
		int foundElements = driver
				.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class, 'v-grid-row')]")).size();
		Assert.assertEquals(1, foundElements);
	}

}
